import about from "./about.jpg";
import email from "./email.jpg";
import chat from "./chat.jpg";
import home from "./home.jpg";
import offer from "./offer.png";
import orderOverview from "./order-overview.jpg";
import productHome from "./product-home.png";
import Home from "./home.png";
import LogoATM from "./logo-atm.png";
import widgets2 from "./widgets-2.jpg";
import widgets3 from "./widgets-3.jpg";
import widgets4 from "./widgets-4.jpg";
import widgets5 from "./widgets-5.jpg";
import widgets6 from "./widgets-6.jpg";
import widgets7 from "./widgets-7.jpg";
import widgets from "./widgets.jpg";

export {
  about,
  email,
  chat,
  home,
  offer,
  orderOverview,
  productHome,
  Home,
  LogoATM,
  widgets2,
  widgets3,
  widgets4,
  widgets5,
  widgets6,
  widgets7,
  widgets
};

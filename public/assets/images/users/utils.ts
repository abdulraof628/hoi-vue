import avatar1 from "./avatar-1.png";
import avatar2 from "./avatar-2.png";
import avatar3 from "./avatar-3.png";
import avatar4 from "./avatar-4.png";
import avatar5 from "./avatar-5.png";
import avatar6 from "./avatar-6.png";
import avatar7 from "./avatar-7.png";
import avatar8 from "./avatar-8.png";
import avatar9 from "./avatar-9.png";
import avatar10 from "./avatar-10.png";
import multiUser from "./multi-user.jpg";
import user1 from "./user-1.jpg";
import user2 from "./user-2.jpg";
import user3 from "./user-3.jpg";
import user4 from "./user-4.jpg";
import userDummyImg from "./user-dummy-img.jpg";
import userProfile from "./user-profile.png";

export {
  avatar1,
  avatar2,
  avatar3,
  avatar4,
  avatar5,
  avatar6,
  avatar7,
  avatar8,
  avatar9,
  avatar10,
  multiUser,
  user1,
  user2,
  user3,
  user4,
  userDummyImg,
  userProfile
};

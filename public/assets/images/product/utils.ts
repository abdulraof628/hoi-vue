import bg from "./bg.png";
import cta2 from "./cta-2.png";
import cta from "./cta.png";
import img01 from "./img-01.png";
import img02 from "./img-02.png";
import img03 from "./img-03.png";
import img04 from "./img-04.png";
import img05 from "./img-05.png";
import img06 from "./img-06.png";
import img07 from "./img-07.png";
import img08 from "./img-08.png";
import img09 from "./img-09.png";
import img10 from "./img-10.png";
import img11 from "./img-11.png";
import img12 from "./img-12.png";
import img13 from "./img-13.png";
import img14 from "./img-14.png";
import img15 from "./img-15.png";
import img16 from "./img-16.png";
import img17 from "./img-17.png";
import img18 from "./img-18.png";
import img19 from "./img-19.png";
import overview01 from "./overview-01.png";
import overview02 from "./overview-02.png";

export {
  bg,
  cta2,
  cta,
  img01,
  img02,
  img03,
  img04,
  img05,
  img06,
  img07,
  img08,
  img09,
  img10,
  img11,
  img12,
  img13,
  img14,
  img15,
  img16,
  img17,
  img18,
  img19,
  overview01,
  overview02
};

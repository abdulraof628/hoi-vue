import './bootstrap';
import '../../public/assets/scss/tailwind.scss';
import '../../public/assets/scss/icons.scss';

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/vue3';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import { ZiggyVue } from '../../vendor/tightenco/ziggy/dist/vue.m';
import TList from "@/Components/External/list/index.vue";
import TDrawer from "@/Components/External/drawer/index.vue";
import TMenu from "@/Components/External/Menu.vue";
import TTabs from "@/Components/External/tabs/index.vue";
import TTab from "@/Components/External/tabs/Tab.vue";
import TCard from "@/Components/External/Card.vue";
import TAvatar from "@/Components/External/avatar/Default.vue";
import TAlert from "@/Components/External/Alert.vue";
import TAvatarGroup from "@/Components/External/avatar/Group.vue";
import TAvatarGroupWithTooltip from "@/Components/External/avatar/GroupWithTooltip.vue";
import TButton from "@/Components/External/Button.vue";
import TLabel from "@/Components/External/Label.vue";
import TCollapse from "@/Components/External/Collapse.vue";
import TCountTo from "@/Components/External/CountTo.vue";
import TNotification from "@/Components/External/Notification.vue";
import TModal from "@/Components/External/Modal.vue";
import TProgressBar from "@/Components/External/ProgressBar.vue";
import TBasicTable from "@/Components/External/tables/Basic.vue";
import TDataTable from "@/Components/External/tables/DataTable.vue";
import TListJsTable from "@/Components/External/tables/ListJs.vue";
import TInputField from "@/Components/External/formFields/InputField.vue";
import TFlatPicker from "@/Components/External/datePicker/FlatPicker.vue";
import TPagination from "@/Components/External/Pagination.vue";
import TTextarea from "@/Components/External/formFields/Textarea.vue";
import TInputRange from "@/Components/External/formFields/InputRange.vue";
import TValidationInputField from "@/Components/External/formFields/ValidationInputField.vue";
import TSelect from "@/Components/External/formFields/Select.vue";
import TCheckbox from "@/Components/External/formFields/Checkbox.vue";
import TRadio from "@/Components/External/formFields/Radio.vue";
import TSwitch from "@/Components/External/formFields/Switch.vue";
import TValidationFlatPicker from "@/Components/External/datePicker/ValidationFlatPicker.vue";
import TFileUploader from "@/Components/External/FileUploader.vue";
import TColorPicker from "@/Components/External/ColorPicker.vue";
import TMultiSelect from "@/Components/External/MultiSelect.vue";
import NumberInputSpinner from "@/Components/External/NumberInputSpinner.vue";
import TCkEditor from "@/Components/External/editors/CKEditor.vue";
import TBallon from "@/Components/External/editors/Ballon.vue";
import TInline from "@/Components/External/editors/Inline.vue";

const appName = import.meta.env.VITE_APP_NAME || 'Laravel';

createInertiaApp({
    title: (title) => `${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({ el, App, props, plugin }) {
        return createApp({ render: () => h(App, props) })
            .use(plugin)
            .use(ZiggyVue).component("TList", TList)
            .component("TDrawer", TDrawer)
            .component("TMenu", TMenu)
            .component("TTabs", TTabs)
            .component("TTab", TTab)
            .component("TCard", TCard)
            .component("TAlert", TAlert)
            .component("TAvatar", TAvatar)
            .component("TAvatarGroup", TAvatarGroup)
            .component("TAvatarGroupWithTooltip", TAvatarGroupWithTooltip)
            .component("TButton", TButton)
            .component("TLabel", TLabel)
            .component("TCollapse", TCollapse)
            .component("TCountTo", TCountTo)
            .component("TNotification", TNotification)
            .component("TModal", TModal)
            .component("TProgressBar", TProgressBar)
            .component("TBasicTable", TBasicTable)
            .component("TDataTable", TDataTable)
            .component("TListJsTable", TListJsTable)
            .component("TInputField", TInputField)
            .component("TFlatPicker", TFlatPicker)
            .component("TPagination", TPagination)
            .component("TTextarea", TTextarea)
            .component("TInputRange", TInputRange)
            .component("TValidationInputField", TValidationInputField)
            .component("TSelect", TSelect)
            .component("TCheckbox", TCheckbox)
            .component("TRadio", TRadio)
            .component("TSwitch", TSwitch)
            .component("TValidationFlatPicker", TValidationFlatPicker)
            .component("TFileUploader", TFileUploader)
            .component("TColorPicker", TColorPicker)
            .component("TMultiSelect", TMultiSelect)
            .component("TNumberInputSpinner", NumberInputSpinner)
            .component("TCkEditor", TCkEditor)
            .component("TBallon", TBallon)
            .component("TInline", TInline)
            .mount(el)
    },
    progress: {
        color: '#4B5563',
    },
});

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class MaklumatPeribadiController extends Controller
{
    public function index(){
        return Inertia::render('Modules/Peribadi/Index');
    }
}
